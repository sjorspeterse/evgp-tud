# Description

_Give a brief explanation of what the issue is or what needs to implemented. Think of the task(s) that needs to be done._  
_If you can divide it into multiple, smaller, separate tasks, then please create separate issues for them!_

# Definition of Done

_Here goes the definition of done of this specific issue. You can make a checklist in which you define all the criteria a feature has to follow for it to be considered done. Here is an example checklist:_

- [ ] Make this thing functional
- [ ] Be able to do this
- [ ] Make the formatting look good

_Note: you can check these checboxes by writing an 'x' in the checkbox when editing, so like this '[x]'._

# Time estimation [remove this header]

_Write a rough estimate of time you think you need to complete this entire issue. Please make sure you think about this, as you need to reflect on this in the sprint retrospective!_

# IMPORTANT: assignees [remove this header]

_Add all people that work on this issue to the contributors of this issue. You can do this by selecting all assignees in the **assignees** tab on GitLab on this issue's page. Or you can leave it blank and later assign yourself to the issue later. This section only serves as a reminder/explanation, it needs to be removed when creating the actual issue._
