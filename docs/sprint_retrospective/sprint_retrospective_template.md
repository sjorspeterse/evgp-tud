# GROUP 7C - Sprint Retrospective, Iteration ... [day-month-year till day-month-year] <!-- e.g. iteration 1, 02-05-22 till 08-05-22 -->

| User Story #          | Task #                      | Task assigned to | Estimated Effort per task (in hours) | Actual Effort (in hours) | Done (yes/no) | Notes                                                                                                |
| --------------------- | --------------------------- | ---------------- | ------------------------------------ | ------------------------ | ------------- | ---------------------------------------------------------------------------------------------------- |
| User story 1          | Task 1                      | Group Members    | Relative Effort                      | Actual Effort            | Yes/No        | ...                                                                                                  |
| User is able to login | Endpoint for authentication | Andy Li          | 4 hours                              | 4.5 hours                | Yes           | This serves as just an example of the format, please remove this for actual sprint-retrospectives... |

# Main problems encountered

### Problem 1

Description: <!-- Describe problem encountered. -->

Reaction: <!-- Describe our reaction to it. -->

### Problem 2

Description: <!-- Describe problem encountered. -->

Reaction: <!-- Describe our reaction to it. -->

# Adjustments for the next Sprint plan

_Motivate any adjustments that will be made for the next Sprint Plan._
