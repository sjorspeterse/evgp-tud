## Meeting Notes TA Meeting Week 2

<!-- change week number accordingly!! -->

|                | Description                                                     |
| -------------- | --------------------------------------------------------------- | ----------------------------------- |
| **Location**   | TBD                                                             | <!-- e.g. Drebbelweg PC-Hall 2 -->  |
| **Date**       | 25-04-22                                                        | <!-- e.g. 22-02-22 -->              |
| **Time**       | 13:45-14:15                                                     | <!-- e.g. 9:00-9:30 -->             |
| **Main focus** | Setting up the repository and getting started with the project. | <!-- e.g. setting up repository --> |
| **Chair**      | Andy Li                                                         | <!-- e.g. Bob West -->              |
| **Note taker** | Jan Domhof                                                      | <!-- e.g. Charlie North -->         |

# Attendees

| Name                  | Role               |
| --------------------- | ------------------ |
| **Stefan Hugtenburg** | Coach              |
| **Naval Bhagat**      | Teaching Assistant |
| **Maroje Borsic**     | Developer          |
| **Bas Filius**        | Developer          |
| **Jan Domhof**        | Developer          |
| **Andy Li**           | Developer          |
| **Vladimir Rullens**  | Developer          |

# Agenda-items

# Opening

<!-- Start the meeting -->

- Officially started the meeting at 13:52

# Check-in (mandatory!)

- Everyone except Maroje attended the meeting.

# Reflection on feedback last week (mandatory!) [start-end time]

<!-- What did we do with the feedback? -->

- _Not applicable this week._

# Points of action

<!-- Items discussed during the meeting, sorted on importance -->

## Setting up repository

- How to set up the GitLab repository, since there is only the group for our project made, but we cannot actually create files etc. A: Already set up, all we need to do is to mirror/clone/copy Sjors' repo into our own. TA can help/do this for us.
- How about the (general) pipeline? A: We can either copy the existing pipeline from our client or we can create it ourselves, but we can ask for help from the TA if we run into problems.
- Do we need to set up Docker? If so, how to set up Docker?/Can we get some help for setting up Docker? A: If the client already has a docker image and he wants us to use it, we should. We might need to change a few things in order to run it on the TU Delft servers. Otherwise we will need to create it ourselves, but we will be able to recieve help from the TA.
- The client already has some code on GitHub, do we clone/duplicate this on our own GitLab page? A: Depends on the preference of the client.

## Questions about the agenda (items)

- What to do with the agenda topics:
  - **'Reflection on feedback last week (mandatory!)'**
  - **'Feedback round (mandatory!)'**: _giving tips and tops to the team, code of conduct still followed etc._\
    Should we discuss this during our meeting, since this might not be interesting for the TA or is this mandatory to discuss during our meetings? A: We should discuss the feedback beforehand and also make a plan on how to improve based on the feedback. We can then discuss this during the meeting.
- Should we make an agenda only for the TA meetings or also for client meetings/ our own personal meetings?
  - In other words: which ones get graded? A: The agenda will not be graded. What will be graded is the preparedness and how we interact with the TA and coach.

## Other items that were discussed

### Coach

- Our coach, Stefan, had some points about his role during this project. First of all, he will review our project plan at the end of week 2. He will also attend both our presentations. He would like to do a bit more, and he mentioned that we could always ask him questions via email or mattermost.

### Presentations

- The presentations will be done in hybrid mode, so that Nabil will be able to watch the presentation as well. Stefan will reserve a presentation room that facilitates hybrid presentations.

# Summary action points

<!-- Note: below is just an example of the format! -->

| Who              | What              | When                     |
| ---------------- | ----------------- | ------------------------ |
| All team members | Set up repository | Monday 25-04-22 at 14:30 |

# Feedback round (mandatory!)

<!-- Give tips and tops to the team. Are the agreements in the code of conduct still followed? -->

- No feedback round for now.

# Question round

- No additional question were asked.

# Closing

- Meeting has ended at 14:06
