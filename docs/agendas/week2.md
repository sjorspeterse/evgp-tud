## Agenda TA Meeting Week 2
<!-- change week number accordingly!! -->

|                | Description                                                     |
|----------------|-----------------------------------------------------------------|
| **Location**   | VMB (building 28) in room Mirzakhani                            | <!-- e.g. Drebbelweg PC-Hall 2 -->
| **Date**       | 25-04-22                                                        | <!-- e.g. 22-02-22 -->
| **Time**       | 13:45-14:15                                                     | <!-- e.g. 9:00-9:30 -->
| **Main focus** | Setting up the repository and getting started with the project. | <!-- e.g. setting up repository -->
| **Chair**      | Andy Li                                                         | <!-- e.g. Bob West -->
| **Note taker** | Jan Domhof                                                      | <!-- e.g. Charlie North -->

# Attendees
| Name                  | Role               |
|-----------------------|--------------------|
| **Naval Bhagat**      | Teaching Assistant |
| **Stefan Hugtenburg** | Supervisor         |
| **Maroje Borsic**     | Developer          |
| **Bas Filius**        | Developer          |
| **Jan Domhof**        | Developer          |
| **Andy Li**           | Developer          |
| **Vladimir Rullens**  | Developer          |

# Agenda-items
# Opening [13:45-13:45]
<!-- Start the meeting -->
- Officially start the meeting.


# Check-in (mandatory!) [13:45-13:46]
- Check if all the attendees are present.


# Reflection on feedback last week (mandatory!) [start-end time]
<!-- What did we do with the feedback? -->
- _Not applicable this week._


# Points of action
<!-- Items discussed during the meeting, sorted on importance -->
## Setting up repository [13:46-14:00]
- How to set up the GitLab repository, since there is only the group for our project made, but we cannot actually create files etc.
- How about the (general) pipeline?
- Do we need to set up Docker? If so, how to set up Docker?/Can we get some help for setting up Docker?
- The client already has some code on GitHub, do we clone/duplicate this on our own GitLab page?

## Questions about the agenda (items) [14:00-14:05]
- What to do with the agenda topics:
    - **'Reflection on feedback last week (mandatory!)'**
    - **'Feedback round (mandatory!)'**: _giving tips and tops to the team, code of conduct still followed etc._\
      Like should we discuss this during our meeting, since this might not be interesting for the TA or is this mandatory to discuss during our meetings?
- Should we make an agenda only for the TA meetings or also for client meetings/ our own personal meetings?
    - In other words: which ones get graded?



# Summary action points [14:05-14:07]
<!-- Note: below is just an example of the format! -->
| Who              | What              | When                     |
|------------------|-------------------|--------------------------|
| All team members | Set up repository | Monday 25-04-22 at 14:30 |




# Feedback round (mandatory!) [start-end time]
<!-- Give tips and tops to the team. Are the agreements in the code of conduct still followed? -->
- Do we need to do this?


# Question round [14:05-14:15]
- Allow team members to ask questions if there are any.


# Closing [14:15-14:15]
- End the meeting.