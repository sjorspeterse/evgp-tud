## Agenda TA Meeting Week 2
<!-- change week number accordingly!! -->

|                | Description |
|----------------|-------------|
| **Location**   | ...         | <!-- e.g. Drebbelweg PC-Hall 2 -->
| **Date**       | ...         | <!-- e.g. 22-02-22 -->
| **Time**       | ...         | <!-- e.g. 9:00-9:30 -->
| **Main focus** | ...         | <!-- e.g. setting up repository -->
| **Chair**      | ...         | <!-- e.g. Bob West -->
| **Note taker** | ...         | <!-- e.g. Charlie North -->

# Attendees
| Name                 | Role               |
|----------------------|--------------------|
| **Naval Bhagat**     | Teaching Assistant |
| **Maroje Borsic**    | Developer          |
| **Bas Filius**       | Developer          |
| **Jan Domhof**       | Developer          |
| **Andy Li**          | Developer          |
| **Vladimir Rullens** | Developer          |

# Agenda-items
# Opening [start-end time]
<!-- Start the meeting -->
- Officially start the meeting.


# Check-in (mandatory!) [start-end time]
- Check if all the attendees are present.


# Reflection on feedback last week (mandatory!) [start-end time]
- What did we do with the feedback?


# Points of action
<!-- Items discussed during the meeting, sorted on importance -->
## Agenda-item 1 [start-end time]
- Agenda topic 1 ...

## Agenda-item 2 [start-end time]
- Agenda topic 2 ...


# Summary action points [start-end time]
<!-- Note: below is just an example of the format! -->
| Who              | What                             | When                     |
|------------------|----------------------------------|--------------------------|
| All team members | Labeling the MoSCoW requirements | Monday 25-04-22 at 14:00 |



# Feedback round (mandatory!) [start-end time]
- Give tips and tops to the team. Are the agreements in the code of conduct still followed?


# Question round [start-end time]
- Allow team members to ask questions if there are any.


# Closing [start-end time]
- End the meeting.